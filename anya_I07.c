define FUSE_USE_VERSION 28
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

static int ANIMEKU = 0;
static int IAN = 1;
static int NAM_DO_SAQ = 2;
static char rootPath[2058];
static char wibuLogPath[2058];
static char innuLogPath[12058];
static char *vigenereKey = "INNUGANTENG";
static int vigenereKeyLength = 11;


void atbash(char str[1000], char newStr[1000]) {
  if (!strcmp(str, ".") || !strcmp(str, "..")) {
    strcpy(newStr, str);
    return;
  };

  int i, flag = 0;
  i = 0;
  while (str[i] != '\0') {
    if (str[i] == '.') {
      flag = 1;
    }
    if (flag == 1) {
      newStr[i] = str[i];
      i++;
      continue;
    }

    if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97) ||
          (str[i] > 122 && str[i] <= 127))) {
      if (str[i] >= 'A' && str[i] <= 'Z') {
        newStr[i] = 'Z' + 'A' - str[i];
      }
      if (str[i] >= 'a' && str[i] <= 'z') {
        newStr[i] = 'z' + 'a' - str[i];
      }
    }

    if (((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97) ||
         (str[i] > 122 && str[i] <= 127))) {
      newStr[i] = str[i];
    }

    i++;
  }
  newStr[i] = '\0';
}

void rot13Enc(char str[1000], char newStr[1000]) {

  if (!strcmp(str, ".") || !strcmp(str, "..")) {
    strcpy(newStr, str);
    return;
  };

  int i, flag = 0;
  i = 0;
  while (str[i] != '\0') {
    // exclude extension
    if (str[i] == '.') {
      flag = 1;
    }
    if (flag == 1) {
      newStr[i] = str[i];
      i++;
      continue;
    }

    if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97))) {
      if (str[i] >= 'A' && str[i] <= 'Z') {
        if (str[i] + 13 > 'Z') {
          newStr[i] = str[i] - 13;
        } else {
          newStr[i] = str[i] + 13;
        }
      }
      if (str[i] >= 'a' && str[i] <= 'z') {
        if (str[i] + 13 > 'z') {
          newStr[i] = str[i] - 13;
        } else {
          newStr[i] = str[i] + 13;
        }
      }
    }

    if ((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97)) {
      newStr[i] = str[i];
    }

    i++;
  }
  newStr[i] = '\0';
}

void rot13Denc(char str[1000], char newStr[1000]) {
  // Skip if directory
  if (!strcmp(str, ".") || !strcmp(str, "..")) {
    strcpy(newStr, str);
    return;
  };

  int i, flag = 0;
  i = 0;
  while (str[i] != '\0') {
    // exclude extension
    if (str[i] == '.') {
      flag = 1;
    }
    if (flag == 1) {
      newStr[i] = str[i];
      i++;
      continue;
    }

    if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97))) {
      if (str[i] >= 'A' && str[i] <= 'Z') {
        if (str[i] - 13 < 'A') {
          newStr[i] = str[i] + 13;
        } else {
          newStr[i] = str[i] - 13;
        }
      }
      if (str[i] >= 'a' && str[i] <= 'z') {
        if (str[i] - 13 < 'a') {
          newStr[i] = str[i] + 13;
        } else {
          newStr[i] = str[i] - 13;
        }
      }
    }

    if ((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97)) {
      newStr[i] = str[i];
    }

    i++;
  }
  newStr[i] = '\0';
}

void vigen(char str[1000], char newStr[1000], bool encr) {
  // Skip if directory
  if (!strcmp(str, ".") || !strcmp(str, "..")) {
    strcpy(newStr, str);
    return;
  };

  char key[] = "SISOP";
  int msgLen = strlen(str), keyLen = strlen(key), i, j;
  char newKey[msgLen];

  // generating new key
  for (i = 0, j = 0; i < msgLen; ++i, ++j) {
    if (j == keyLen) j = 0;

    newKey[i] = key[j];
  }
  newKey[i] = '\0';

  int flag = 0;
  i = 0;
  while (str[i] != '\0') {
    char temp;

    // exclude extension
    if (str[i] == '.') {
      flag = 1;
    }
    if (flag == 1) {
      newStr[i] = str[i];
      i++;
      continue;
    }

    if (encr) {
      if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97))) {
        if (str[i] >= 'A' && str[i] <= 'Z') {
          if (newKey[i] >= 'a' && newKey[i] <= 'z') {
            temp = newKey[i] - 'a' + 'A';
            newStr[i] = ((str[i] + temp) % 26) + 'A';
          } else {
            newStr[i] = ((str[i] + newKey[i]) % 26) + 'A';
          }
        }
        if (str[i] >= 'a' && str[i] <= 'z') {
          temp = str[i] - 'a' + 'A';
          if (newKey[i] >= 'a' && newKey[i] <= 'z') {
            char tempNK = newKey[i] - 'a' + 'A';
            newStr[i] = ((temp + tempNK) % 26) + 'A';
          } else {
            newStr[i] = ((temp + newKey[i]) % 26) + 'A';
          }
          newStr[i] = newStr[i] - 'A' + 'a';
        }
      }
    } else {
      if ((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97)) {
        if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97))) {
          if (str[i] >= 'A' && str[i] <= 'Z') {
            if (newKey[i] >= 'a' && newKey[i] <= 'z') {
              temp = newKey[i] - 'a' + 'A';
              newStr[i] = (((str[i] + temp) + 26) % 26) + 'A';
            } else {
              newStr[i] = (((str[i] + newKey[i]) + 26) % 26) + 'A';
            }
          }
          if (str[i] >= 'a' && str[i] <= 'z') {
            temp = str[i] - 'a' + 'A';
            if (newKey[i] >= 'a' && newKey[i] <= 'z') {
              char tempNK = newKey[i] - 'a' + 'A';
              newStr[i] = (((temp + tempNK) + 26) % 26) + 'A';
            } else {
              newStr[i] = (((temp + newKey[i]) + 26) % 26) + 'A';
            }
            newStr[i] = newStr[i] - 'A' + 'a';
          }
        }
      }
    }

    i++;
  }
  newStr[i] = '\0';
}

void loglvlWarning(const char *log, const char *path) {
  FILE *fp;
  fp = fopen("/Users/Documents/amelia_mumtazah18/Wibu.log", "a");
  fputs("WARNING::", fp);
  char timestamp[1000];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(timestamp, "%02d%02d%04d-%02d:%02d:%02d:", tm.tm_mday, tm.tm_mon + 1,
          tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
  fputs(timestamp, fp);
  fputs(log, fp);
  fputs("::", fp);
  fputs(path, fp);
  fputs("\n", fp);
  fclose(fp);
}

void loglvlInfo(const char *log, const char *path) {
  FILE *fp;
  fp = fopen("/Users/Documents/amelia_mumtazah18/Wibu.log", "a");
  fputs("INFO::", fp);
  char timestamp[1000];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(timestamp, "%02d%02d%04d-%02d:%02d:%02d:", tm.tm_mday, tm.tm_mon + 1,
          tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
  fputs(timestamp, fp);
  fputs(log, fp);
  fputs("::", fp);
  fputs(path, fp);
  fputs("\n", fp);
  fclose(fp);
}

void loglvlInfo2(const char *log, const char *source, const char *destination) {
  FILE *fp;
  fp = fopen("/Users/Documents/amelia_mumtazah18/Wibu.log", "a");
  fputs("INFO::", fp);
  char timestamp[1000];
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  sprintf(timestamp, "%02d%02d%04d-%02d:%02d:%02d:", tm.tm_mday, tm.tm_mon + 1,
          tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
  fputs(timestamp, fp);
  fputs(log, fp);
  fputs("::", fp);
  fputs(source, fp);
  fputs("::", fp);
  fputs(destination, fp);
  fputs("\n", fp);
  fclose(fp);
}
