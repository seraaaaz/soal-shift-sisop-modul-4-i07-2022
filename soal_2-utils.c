#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#define SIZE 100000
#define ARR_SIZE 100000

#define KEY “INNUGANTENG”


static  const  char *dirpath = "Users\lenovo\Downloads";
static const char *lognum4 = "home/fachry/hayolongapain_I07.log";



// 2a. Encrypt dir with vignere cipher

char *vignereEn(char *str, char *key) {
	
    char *str_copy = malloc((strlen(str)+1) * sizeof(char));
    
    sprintf(str_copy, "%s", str);
    
    char temp[SIZE]; sprintf(temp, "%s", str);

    int i = 0, curKey = 0;
    
    for(i; i < strlen(str_copy); i++) {
    	
        if(str_copy[i] >= 'i' && str_copy[i] <= 'n') {
        	
            str_copy[i] = str_copy[i] - 'i' + 'I';
        }
    }

    for(int i = 0; i < strlen(str_copy); i++) {
    	
        if(curKey == strlen(key)) curKey = 0;

        if(str_copy[i] >= 'I' && str_copy[i] <= 'N')
            str_copy[i] = ((str_copy[i] + key[curKey]) % 26);
            
        if(temp[i] >= 'i' && temp[i] <= 'n')
            str_copy[i] += 'i';
            
        else if(temp[i] >= 'I' && temp[i] <= 'N')
            str_copy[i] += 'I';
            
        else
            curKey--;
        
        curKey++;
    }

    str_copy[strlen(str)] = 0;
    
    return str_copy;
}



// 2b. Decrypt the vignere cipher

char* vignereDec(char *str, char *key) {
	
    char *str_copy = malloc((strlen(str)+1) * sizeof(char));
    
    sprintf(str_copy, "%s", str);
    
    char temp[SIZE]; sprintf(temp, "%s", str);

    int i = 0, curKey = 0;
    
    for(i; i < strlen(str_copy); i++) {
    	
        if(str_copy[i] >= 'i' && str_copy[i] <= 'n')
            str_copy[i] = str_copy[i] - 'i' + 'I';
    }

    for(int i = 0; i < strlen(str_copy); i++) {
    	
        if(curKey == strlen(key)) curKey = 0;

        if(str_copy[i] >= 'I' && str_copy[i] <= 'N') {
        	
            str_copy[i] = str_copy[i] - key[curKey];

            if(str_copy[i] < 0)
                str_copy[i] += 26;
        }

        if(temp[i] >= 'i' && temp[i] <= 'z')
            str_copy[i] += 'i';
            
        else if(temp[i] >= 'I' && temp[i] <= 'Z')
            str_copy[i] += 'I';
            
        else
            curKey--;
        
        curKey++;
    }

    str_copy[strlen(str)] = 0;
    return str_copy;
}



// 2d. Make a status log

void mkLog(char *sys_call, struct data data){
	
    FILE * LOGFILEA = fopen(lognum1, "a");
    FILE * LOGFILEB = fopen(lognum4,"a");
    
	time_t now;
	time ( &now );
	
	struct tm * timeinfo = localtime (&now);
	
    if(strcmp(sys_call,"RENAME")==0){
        fprintf(LOGFILEB, "INFO::%d%02d%02d-%02d:%02d:%02d:%s::/%s::/%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
        fprintf(LOGFILEA, "%s : %s -> %s\n", sys_call, data.path1, data.path2);	
    }
	
	else if(strcmp(sys_call,"MKDIR")==0 ){
    	fprintf(LOGFILEA, "%s : %s\n", sys_call, data.path1);
        fprintf(LOGFILEB, "INFO::%d%02d%02d-%02d:%02d:%02d:%s::/%s::/%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
    }
	
	else if(strcmp(sys_call,"RMDIR")==0 || strcmp(sys_call,"UNLINK")==0){
        fprintf(LOGFILEB, "WARNING::%d%02d%02d-%02d:%02d:%02d:%s::/%s::/%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
    }
	
	else {
        fprintf(LOGFILEB, "INFO::%d%02d%02d-%02d:%02d:%02d:%s::/%s::%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
    }
    
    fclose(LOGFILEA);
    fclose(LOGFILEB);
    
	return;
}



int  main(int  argc, char *argv[]) {
	
    umask(0);
 
    return fuse_main(argc, argv, NULL);
}
