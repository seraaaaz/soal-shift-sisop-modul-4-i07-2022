# soal-shift-sisop-modul-4-I07-2022
## **Laporan Resmi dan Penjelasan Soal Shift Modul 2**
### **Kelompok I-07**
**Anggota :**
- Selomita Zhafirah 5025201120
- Amelia Mumtazah Karimah 5025201128
- Mohammed Fachry Dwi Handoko 5025201159

## **Daftar Isi Lapres**
- [Soal-1](#soal1)
- [Soal-2](#soal2)
- [Soal-3](#soal3)

# Soal 1
## **1a**
Inti dari soal 1a adalah membuat semua direktori dengan awalan “Animeku_” akan terencode dengan ketentuan semua file yang terdapat huruf besar akan ter encode dengan atbash cipher dan huruf kecil akan terencode dengan rot13. 

Langkah pertama yaitu kami membuat beberapa deklarasi library untuk menjalankan program  dan integer.
```bash
#define FUSE_USE_VERSION 28
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
```
Lalu membuat struktur fuse 
```bash
static int ANIMEKU = 0;
static int IAN = 1;
static int NAM_DO_SAQ = 2;
static char rootPath[2058];
static char wibuLogPath[2058];
static char innuLogPath[12058];
static char *vigenereKey = "INNUGANTENG";
static int vigenereKeyLength = 11;
```

Kemudian yang harus dilakukan adalah dengan membuat fungsi encode terlebih dahulu, karena enkripsi atbash adalah enkripsi yang bisa berlaku 2 arah untuk encode dan decode, maka kita hanya perlu menggunakan 1 fungsi saja.
```bash
void atbash(char str[1000], char newStr[1000]) {
  // Skip if directory
  if (!strcmp(str, ".") || !strcmp(str, "..")) {
    strcpy(newStr, str);
    return;
  };

  int i, flag = 0;
  i = 0;
  while (str[i] != '\0') {
    // exclude extension
    if (str[i] == '.') {
      flag = 1;
    }
    if (flag == 1) {
      newStr[i] = str[i];
      i++;
      continue;
    }

    if (!((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97) ||
          (str[i] > 122 && str[i] <= 127))) {
      if (str[i] >= 'A' && str[i] <= 'Z') {
        newStr[i] = 'Z' + 'A' - str[i];
      }
      if (str[i] >= 'a' && str[i] <= 'z') {
        newStr[i] = 'z' + 'a' - str[i];
      }
    }

    if (((str[i] >= 0 && str[i] < 65) || (str[i] > 90 && str[i] < 97) ||
         (str[i] > 122 && str[i] <= 127))) {
      newStr[i] = str[i];
    }

    i++;
  }
  newStr[i] = '\0';
}
```
## **1b**
Inti dari soal 1b adalah membuat semua direktori di-rename dengan awalan “Animeku_”, maka direktori tersebut akan menjadi direktori ter-encode dengan ketentuan sama dengan 1a.

Kemudian, dari fungsi sebelumnya yang bertugas untuk memirror string, maka kita harus menentukan apakah folder tersebut harus kita encode dengan menggunakan strncmp(token, "Animeku_", 5) == 0. Pengecekan di atas dilakukan pada fungsi xmp_readdir sehingga kita bisa mengubah nama yang ditampilkan pada file system fuse.
```bash
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                       off_t offset, struct fuse_file_info *fi) {
  char fpath[1000];
  printf("\n🚀 READDIR\n");

  int hasToEncrypt = 0;
  char currPath[1000] = "", pathEnc[1000] = "";

  char *token = strtok(path, "/");
    while (token != NULL) {
        if (hasToEncrypt) {
          strcat(pathEnc, "/");
          strcat(pathEnc, token);
    }    else if (!hasToEncrypt) {
          strcat(currPath, "/");
          strcat(currPath, token);
    }

        if (strncmp(token, "Animeku_", 5) == 0) {
           hasToEncrypt = 1;
    }   else if (strncmp(token, "RX_", 3) == 0) {
           hasToEncrypt = 2;
    }
    token = strtok(NULL, "/");
  }

        if (strcmp(path, "/") == 0) {
          path = dirpath;
          sprintf(fpath, "%s", path);
  }     else {
            char pathDec[1000], pathDec2[1000];
         if (hasToEncrypt == 1) {
          atbash(pathEnc, pathDec);
          sprintf(fpath, "%s%s%s", dirpath, path, pathDec);
    }     
        else if (hasToEncrypt == 2) {
          rot13Denc(pathEnc, pathDec2);
          atbash(pathDec2, pathDec);
          sprintf(fpath, "%s%s%s", dirpath, path, pathDec);
    }   else {
          sprintf(fpath, "%s%s", dirpath, path);
    }
  }

  int res = 0;

  DIR *dp;

  struct dirent *de;
  (void)offset;
  (void)fi;
  printf("temp send: %s\n", fpath);

  dp = opendir(fpath);

      if (dp == NULL) return -errno;

      while ((de = readdir(dp)) != NULL) {
        struct stat st;

       memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

     char temp[1000], temp2[1000];

     if (hasToEncrypt == 1) {
      atbash(de->d_name, temp);
    } else if (hasToEncrypt == 2) {
      atbash(de->d_name, temp2);
      rot13Enc(temp2, temp);
    } else {
      strcpy(temp, de->d_name);
    }

    printf("temp send: %s\n", temp);

    res = (filler(buf, temp, &st, 0));

    if (res != 0) break;
  }

  closedir(dp);

  loglvlInfo("READDIR", path);

  return 0;
}
```

Pada fungsi ini akan melakukan pengecekan dan melakukan pengecekan itu untuk sub directory" lainnya. Karena dibutuhkan untuk bisa bekerja secara rekursif.Setelah kita mengetahui bahwa folder tersebut harus di encode, maka kita menyimpan pathEnc dan memberikan flag untuk menandakan.

## **1c**
Inti dari soal 1c adalah jika direktori yang terenkripsi di-rename menjadi tidak ter-encode, maka isi direktori tersebut akan terdecode. Maka setelah itu kita membuka directory tersebut, dan mengencode de->d_name dengan algoritma atbash.
```bash
if (strcmp(path, "/") == 0) {
    path = dirpath;
    sprintf(fpath, "%s", path);
  } else {
    char pathDec[1000], pathDec2[1000];
    if (hasToEncrypt == 1) {
      atbash(pathEnc, pathDec);
      sprintf(fpath, "%s%s%s", dirpath, path, pathDec);
    }
    // ...
    else {
      strcpy(temp, de->d_name);
    }
    // ...
```
Pada percabangan ini, jika path merupakan atau maka bisa diskip, dan jika path memiliki awalan Animeku, maka kita harus decode terlebih dahulu menggunakan fungsi atbash, hal ini karena setelah ini kita akan memasukkan path tersebut ke opendir, maka dibutuhkan path yang sebenarnya sehingga file bisa membaca directorynya.
## **1d**
Inti dari soal 1d adalah setiap data yang sudah terencode akan masuk dalam file “Wibu.log”. Pencatatan dilakukan pada ```bash xmp_mkdir``` dan ```bash xmp_rename``` menggunakan fungsi.

```bash
loglvlInfo("MKDIR", path);

loglvlInfo2("RENAME", fpathFrom, fpathTo);
```
## **1e**

<br><br><br>

# Soal 2

<br>

## 2a && 2b
<p align="left">
  Make a directory with the name "IAN_[nama]", and encrypt it with a case-sensitive Vigenere Cipher;
  SECRET_KEY=INNUGANTENG.
  If the directory name is "IAN_[nama]", encode the contents and use the key to view them.
</p>

<br>

```bash
char *vignereEn(char *str, char *key) {
	
    char *str_copy = malloc((strlen(str)+1) * sizeof(char));
    
    sprintf(str_copy, "%s", str);
    
    char temp[SIZE]; sprintf(temp, "%s", str);

    int i = 0, curKey = 0;
    
    for(i; i < strlen(str_copy); i++) {
    	
        if(str_copy[i] >= 'i' && str_copy[i] <= 'n') {
        	
            str_copy[i] = str_copy[i] - 'i' + 'I';
        }
    }

    for(int i = 0; i < strlen(str_copy); i++) {
    	
        if(curKey == strlen(key)) curKey = 0;

        if(str_copy[i] >= 'I' && str_copy[i] <= 'N')
            str_copy[i] = ((str_copy[i] + key[curKey]) % 26);
            
        if(temp[i] >= 'i' && temp[i] <= 'n')
            str_copy[i] += 'i';
            
        else if(temp[i] >= 'I' && temp[i] <= 'N')
            str_copy[i] += 'I';
            
        else
            curKey--;
        
        curKey++;
    }

    str_copy[strlen(str)] = 0;
    
    return str_copy;
}
```

## 2c

<p align="left">
  If the directory is renamed by removing the term "IAN_", then decode the folder and return the internal contents to its
  real / original state.
</p>

```bash
char* vignereDec(char *str, char *key) {
	
    char *str_copy = malloc((strlen(str)+1) * sizeof(char));
    
    sprintf(str_copy, "%s", str);
    
    char temp[SIZE]; sprintf(temp, "%s", str);

    int i = 0, curKey = 0;
    
    for(i; i < strlen(str_copy); i++) {
    	
        if(str_copy[i] >= 'i' && str_copy[i] <= 'n')
            str_copy[i] = str_copy[i] - 'i' + 'I';
    }

    for(int i = 0; i < strlen(str_copy); i++) {
    	
        if(curKey == strlen(key)) curKey = 0;

        if(str_copy[i] >= 'I' && str_copy[i] <= 'N') {
        	
            str_copy[i] = str_copy[i] - key[curKey];

            if(str_copy[i] < 0)
                str_copy[i] += 26;
        }

        if(temp[i] >= 'i' && temp[i] <= 'n')
            str_copy[i] += 'i';
            
        else if(temp[i] >= 'I' && temp[i] <= 'N')
            str_copy[i] += 'I';
            
        else
            curKey--;
        
        curKey++;
    }

    str_copy[strlen(str)] = 0;
    return str_copy;
}
```

<br>

## 2d
<p align="left">
  Make a log to review the status of the entire process for everytime the file system is in use or operating (run). This is
  stored in the directory “/home/[user]/hayolongapain_[kelompok].log”. 
</p>
<br>

```bash
void mkLog(char *sys_call, struct data data){
	
    FILE * LOGFILEA = fopen(lognum1, "a");
    FILE * LOGFILEB = fopen(lognum4, "a");
    
	time_t now;
	time ( &now );
	
	struct tm * timeinfo = localtime (&now);
	
    if(strcmp(sys_call,"RENAME")==0){
        fprintf(LOGFILEB, "INFO::%d%02d%02d-%02d:%02d:%02d:%s::/%s::/%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
        fprintf(LOGFILEA, "%s : %s -> %s\n", sys_call, data.path1, data.path2);	
    }
	
	else if(strcmp(sys_call,"MKDIR")==0 ){
    	fprintf(LOGFILEA, "%s : %s\n", sys_call, data.path1);
        fprintf(LOGFILEB, "INFO::%d%02d%02d-%02d:%02d:%02d:%s::/%s::/%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
    }
	
	else if(strcmp(sys_call,"RMDIR")==0 || strcmp(sys_call,"UNLINK")==0){
        fprintf(LOGFILEB, "WARNING::%d%02d%02d-%02d:%02d:%02d:%s::/%s::/%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
    }
	
	else {
        fprintf(LOGFILEB, "INFO::%d%02d%02d-%02d:%02d:%02d:%s::/%s::%s\n",timeinfo->tm_mday, timeinfo->tm_mon+1, timeinfo->tm_year+1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, sys_call, data.path1, data.path2);
    }
    
    fclose(LOGFILEA);
    fclose(LOGFILEB);
    
	return;
}
```
